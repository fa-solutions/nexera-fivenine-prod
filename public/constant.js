const ALL_CALLS_VIEW = "/entity/five9/view/34f476de-4966-4324-8fa2-3d31cc819df5";

const APPLET_ID = "aHR0cHM6Ly9maXZlOS1hcHBsZXQuczMudXMtZWFzdC0yLmFtYXpvbmF3cy5jb20vcHVibGljL2luZGV4Lmh0bWw="; // aHR0cDovL2xvY2FsaG9zdDo1MDAw

const SERVICE_NAME = "FreeAgentService";


const LOG_SAMPLE = {
    "interactionLogData": {
        "who": {
            "id": "123",
            "label": "Contact",
            "name": "Joe",
            "isWho": true,
            "isWhat": false,
            "fields": [
                {
                    "displayName": "Company",
                    "value": "ABC"
                }
            ],
            "visitedTime": 1622493947472
        },
        "subject": "Call 05/31/2021 01:45pm PDT",
        "comment": null,
        "comments": null,
        "disposition": {
            "id": "0",
            "name": "No Disposition",
            "description": "Default disposition used when no dispositions are defined for a campaign.",
            "flags": [],
            "channelTypes": {
                "CALL": {
                    "filters": [
                        "VOICE_TRANSFER_CALL",
                        "VOICE_DISPOSE_PREVIEW_CALL",
                        "VOICE_END_CONFERENCE",
                        "VOICE_DISPOSE_CALL_OR_VOICEMAIL",
                        "VOICE_STANDARD_DISPOSITION"
                    ]
                }
            },
            "timeout": 0,
            "numberForTransfersAndConferences": null,
            "type": "FINAL"
        },
        "dispositionName": "No Disposition",
        "startTime": 1622493947322,
        "duration": 47000,
        "handleTime": 44419,
        "wrapTime": 44419,
        "talkTime": 0,
        "holdTime": 0,
        "cavList": [
            {
                "id": "41",
                "name": "session_id",
                "group": "Call",
                "type": "STRING",
                "value": "925FEF985E714585AA18D1A5F3A01C6A"
            },
            {
                "id": "42",
                "name": "call_id",
                "group": "Call",
                "type": "STRING",
                "value": "25"
            },
            {
                "id": "43",
                "name": "type",
                "group": "Call",
                "type": "STRING",
                "value": "4"
            },
            {
                "id": "44",
                "name": "type_name",
                "group": "Call",
                "type": "STRING",
                "value": "Manual"
            },
            {
                "id": "48",
                "name": "ANI",
                "group": "Call",
                "type": "STRING",
                "value": "4803936603"
            },
            {
                "id": "49",
                "name": "DNIS",
                "group": "Call",
                "type": "STRING",
                "value": "9871030622"
            },
            {
                "id": "50",
                "name": "disposition_id",
                "group": "Call",
                "type": "STRING",
                "value": "0"
            },
            {
                "id": "51",
                "name": "disposition_name",
                "group": "Call",
                "type": "STRING",
                "value": "No Disposition"
            },
            {
                "id": "52",
                "name": "length",
                "group": "Call",
                "type": "TIME_PERIOD",
                "value": "47872"
            },
            {
                "id": "53",
                "name": "handle_time",
                "group": "Call",
                "type": "TIME_PERIOD",
                "value": "44419"
            },
            {
                "id": "54",
                "name": "wrapup_time",
                "group": "Call",
                "type": "TIME_PERIOD",
                "value": "44419"
            },
            {
                "id": "55",
                "name": "start_timestamp",
                "group": "Call",
                "type": "DATE_TIME",
                "value": "2021-05-31 20:45:47.175"
            },
            {
                "id": "56",
                "name": "end_timestamp",
                "group": "Call",
                "type": "DATE_TIME",
                "value": "2021-05-31 20:46:35.189"
            },
            {
                "id": "59",
                "name": "queue_time",
                "group": "Call",
                "type": "TIME_PERIOD",
                "value": "0"
            },
            {
                "id": "60",
                "name": "hold_time",
                "group": "Call",
                "type": "TIME_PERIOD",
                "value": "0"
            },
            {
                "id": "61",
                "name": "park_time",
                "group": "Call",
                "type": "TIME_PERIOD",
                "value": "0"
            },
            {
                "id": "62",
                "name": "bill_time",
                "group": "Call",
                "type": "TIME_PERIOD",
                "value": "47872"
            },
            {
                "id": "63",
                "name": "number",
                "group": "Call",
                "type": "STRING",
                "value": "9871030622"
            },
            {
                "id": "67",
                "name": "domain_id",
                "group": "Call",
                "type": "STRING",
                "value": "133277"
            },
            {
                "id": "68",
                "name": "domain_name",
                "group": "Call",
                "type": "STRING",
                "value": "Newfi Lending Wholesale"
            },
            {
                "id": "81",
                "name": "id",
                "group": "Agent",
                "type": "STRING",
                "value": "3828226"
            },
            {
                "id": "82",
                "name": "user_name",
                "group": "Agent",
                "type": "STRING",
                "value": "ivan.carrillo@freeagentsoftware.com"
            },
            {
                "id": "83",
                "name": "full_name",
                "group": "Agent",
                "type": "STRING",
                "value": "Ivan Carrillo"
            },
            {
                "id": "84",
                "name": "first_agent",
                "group": "Agent",
                "type": "STRING",
                "value": "3828226"
            },
            {
                "id": "85",
                "name": "station_id",
                "group": "Agent",
                "type": "STRING",
                "value": "3406575"
            },
            {
                "id": "86",
                "name": "station_type",
                "group": "Agent",
                "type": "STRING",
                "value": "SOFTPHONE"
            },
            {
                "id": "89",
                "name": "error_code",
                "group": "IVR",
                "type": "NUMBER",
                "value": "0"
            },
            {
                "id": "98",
                "name": "total_body_bytes_size",
                "group": "Omni",
                "type": "NUMBER",
                "value": "0"
            },
            {
                "id": "99",
                "name": "total_body_chars_size",
                "group": "Omni",
                "type": "NUMBER",
                "value": "0"
            },
            {
                "id": "104",
                "name": "email_priority",
                "group": "Omni",
                "type": "NUMBER",
                "value": "0"
            }
        ],
        "crmList": [
            {
                "id": "1018",
                "name": "number1",
                "dataType": "PHONE",
                "restrictions": {
                    "restrictions": [],
                    "limitedSetInfo": null
                },
                "primary": true,
                "value": "9871030622"
            }
        ]
    },
    "interactionType": "Call",
    "interactionData": {
        "agent": "ivan.carrillo@freeagentsoftware.com",
        "agentName": "Ivan Carrillo",
        "agentExtension": "0001",
        "ani": "4803936603",
        "dnis": "9871030622",
        "number": "9871030622",
        "contactDisplayName": "9871030622",
        "click2DialData": null,
        "callType": "AGENT",
        "callId": "25",
        "campaignName": "[None]",
        "interactionId": "FB0EE92E5018476786745B6DF53CC580",
        "sessionId": "925FEF985E714585AA18D1A5F3A01C6A",
        "callbackNumber": null,
        "campaignId": null,
        "callbackId": null,
        "distributionMode": "Automatic",
        "type": "Call"
    }
}


let SCREEN_POP_SAMPLE = {
    "crmObject": {
        "id": "123",
        "label": "Contact",
        "name": "Joe",
        "isWho": true,
        "isWhat": false,
        "fields": [
            {
                "displayName": "Company",
                "value": "ABC"
            }
        ],
        "visitedTime": 1622493947472
    },
    "force": false,
    "interactionType": "Call",
    "interactionData": {
        "agent": "ivan.carrillo@freeagentsoftware.com",
        "agentName": "Ivan Carrillo",
        "agentExtension": "0001",
        "ani": "4803936603",
        "dnis": "9871030622",
        "number": "9871030622",
        "contactDisplayName": "9871030622",
        "click2DialData": null,
        "callType": "AGENT",
        "callId": "25",
        "campaignName": "[None]",
        "interactionId": "FB0EE92E5018476786745B6DF53CC580",
        "sessionId": "925FEF985E714585AA18D1A5F3A01C6A",
        "callbackNumber": null,
        "campaignId": null,
        "callbackId": null,
        "distributionMode": "Automatic",
        "type": "Call"
    }
}


let SEARCH_SAMPLE = {
    "interactionSearchData": {
        "cavList": [
            {
                "id": "41",
                "name": "session_id",
                "group": "Call",
                "type": "STRING",
                "value": "925FEF985E714585AA18D1A5F3A01C6A"
            },
            {
                "id": "42",
                "name": "call_id",
                "group": "Call",
                "type": "STRING",
                "value": "25"
            },
            {
                "id": "43",
                "name": "type",
                "group": "Call",
                "type": "STRING",
                "value": "4"
            },
            {
                "id": "44",
                "name": "type_name",
                "group": "Call",
                "type": "STRING",
                "value": "Manual"
            },
            {
                "id": "48",
                "name": "ANI",
                "group": "Call",
                "type": "STRING",
                "value": "4803936603"
            },
            {
                "id": "49",
                "name": "DNIS",
                "group": "Call",
                "type": "STRING",
                "value": "9871030622"
            },
            {
                "id": "52",
                "name": "length",
                "group": "Call",
                "type": "TIME_PERIOD",
                "value": "31"
            },
            {
                "id": "53",
                "name": "handle_time",
                "group": "Call",
                "type": "TIME_PERIOD",
                "value": "0"
            },
            {
                "id": "54",
                "name": "wrapup_time",
                "group": "Call",
                "type": "TIME_PERIOD",
                "value": "0"
            },
            {
                "id": "55",
                "name": "start_timestamp",
                "group": "Call",
                "type": "DATE_TIME",
                "value": "2021-05-31 20:45:47.175"
            },
            {
                "id": "59",
                "name": "queue_time",
                "group": "Call",
                "type": "TIME_PERIOD",
                "value": "0"
            },
            {
                "id": "60",
                "name": "hold_time",
                "group": "Call",
                "type": "TIME_PERIOD",
                "value": "0"
            },
            {
                "id": "61",
                "name": "park_time",
                "group": "Call",
                "type": "TIME_PERIOD",
                "value": "0"
            },
            {
                "id": "62",
                "name": "bill_time",
                "group": "Call",
                "type": "TIME_PERIOD",
                "value": "31"
            },
            {
                "id": "63",
                "name": "number",
                "group": "Call",
                "type": "STRING",
                "value": "9871030622"
            },
            {
                "id": "67",
                "name": "domain_id",
                "group": "Call",
                "type": "STRING",
                "value": "133277"
            },
            {
                "id": "68",
                "name": "domain_name",
                "group": "Call",
                "type": "STRING",
                "value": "Newfi Lending Wholesale"
            },
            {
                "id": "81",
                "name": "id",
                "group": "Agent",
                "type": "STRING",
                "value": "3828226"
            },
            {
                "id": "82",
                "name": "user_name",
                "group": "Agent",
                "type": "STRING",
                "value": "ivan.carrillo@freeagentsoftware.com"
            },
            {
                "id": "83",
                "name": "full_name",
                "group": "Agent",
                "type": "STRING",
                "value": "Ivan Carrillo"
            },
            {
                "id": "84",
                "name": "first_agent",
                "group": "Agent",
                "type": "STRING",
                "value": "3828226"
            },
            {
                "id": "85",
                "name": "station_id",
                "group": "Agent",
                "type": "STRING",
                "value": "3406575"
            },
            {
                "id": "86",
                "name": "station_type",
                "group": "Agent",
                "type": "STRING",
                "value": "SOFTPHONE"
            },
            {
                "id": "89",
                "name": "error_code",
                "group": "IVR",
                "type": "NUMBER",
                "value": "0"
            },
            {
                "id": "98",
                "name": "total_body_bytes_size",
                "group": "Omni",
                "type": "NUMBER",
                "value": "0"
            },
            {
                "id": "99",
                "name": "total_body_chars_size",
                "group": "Omni",
                "type": "NUMBER",
                "value": "0"
            },
            {
                "id": "104",
                "name": "email_priority",
                "group": "Omni",
                "type": "NUMBER",
                "value": "0"
            }
        ],
        "crmList": [],
        "phoneNumber": "9871030622"
    },
    "interactionType": "Call",
    "interactionData": {
        "agent": "ivan.carrillo@freeagentsoftware.com",
        "agentName": "Ivan Carrillo",
        "agentExtension": "0001",
        "ani": "4803936603",
        "dnis": "9871030622",
        "number": "9871030622",
        "contactDisplayName": "9871030622",
        "click2DialData": null,
        "callType": "AGENT",
        "callId": "25",
        "campaignName": "[None]",
        "interactionId": "FB0EE92E5018476786745B6DF53CC580",
        "sessionId": "925FEF985E714585AA18D1A5F3A01C6A",
        "callbackNumber": null,
        "campaignId": null,
        "callbackId": null,
        "distributionMode": "Automatic",
        "type": "Call"
    }
}





let CALL_FINISHED_SAMPLE = {
    "reason": "DISPOSITIONED",
    "callData": {
        "agent": "ivan.carrillo@freeagentsoftware.com",
        "agentName": "Ivan Carrillo",
        "agentExtension": "0001",
        "ani": "5752153442",
        "dnis": "9871030622",
        "number": "9871030622",
        "contactDisplayName": "9871030622",
        "click2DialData": null,
        "callType": "AGENT",
        "callId": "70",
        "campaignName": "Test",
        "interactionId": "D58CF0BA6C8740649716E259CFE02143",
        "sessionId": "CFF23EA9FCB142428CBDD260DFB4C864",
        "callbackNumber": null,
        "campaignId": "1137600",
        "callbackId": null,
        "distributionMode": "Automatic",
        "type": "Call"
    },
    "callLogData": {
        "who": {
            "id": "343",
            "label": "Contact",
            "name": "Bob",
            "isWho": true,
            "isWhat": false,
            "fields": [
                {
                    "displayName": "Company",
                    "value": "ABC"
                }
            ],
            "visitedTime": 1622824518721
        },
        "subject": "Call 06/04/2021 09:35am PDT",
        "comment": null,
        "comments": null,
        "disposition": {
            "id": "1130795",
            "name": "Left Voicemail",
            "description": null,
            "flags": [],
            "channelTypes": {
                "CALL": {
                    "filters": [
                        "VOICE_TRANSFER_CALL",
                        "VOICE_DISPOSE_PREVIEW_CALL",
                        "VOICE_END_CONFERENCE",
                        "VOICE_DISPOSE_CALL_OR_VOICEMAIL"
                    ]
                }
            },
            "timeout": 0,
            "numberForTransfersAndConferences": null,
            "type": "FINAL"
        },
        "dispositionName": "Left Voicemail",
        "startTime": 1622824518444,
        "duration": 17328,
        "handleTime": 13934,
        "wrapTime": 13934,
        "talkTime": 0,
        "holdTime": 0,
        "crmList": [
            {
                "id": "1018",
                "name": "number1",
                "dataType": "PHONE",
                "restrictions": {
                    "restrictions": [],
                    "limitedSetInfo": null
                },
                "primary": true,
                "value": "9871030622"
            }
        ],
        "saveLogResult": null
    }
}
