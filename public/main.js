let FAClient = null;
let crmApi = null;
let interactionApi = null;
let currentMatchedContact = null;
let isOutbound = false;
let ongoingCall = false;

const delay = t => new Promise(resolve => setTimeout(resolve, t));

function setCurrentMatchedContact(record) {
    currentMatchedContact = record;
}

const SERVICE = {
    name: SERVICE_NAME,
    appletId: APPLET_ID,
};

startupService();

function startupService() {
    notyf = new Notyf({
        duration: 20000,
        dismissible: true,
        position: {
            x: 'center',
            y: 'bottom',
        },
        types: [
            {
                type: 'info',
                className: 'info-notyf',
                icon: false,
            },
        ],
    });

    FAClient = new FAAppletClient({
        appletId: SERVICE.appletId,
    });

    crmApi = window.Five9.CrmSdk.crmApi();
    crmApi.registerApi({
        getAdtConfig: function (params) {
            let config = {
                providerName: 'FreeAgent CRM ADT adapter',
                myCallsTodayEnabled: true,
                showContactInfo: true
            };
            return Promise.resolve(config);
        },
        search: async function ({interactionData, interactionSearchData}) {
            isOutbound = true;
            let phoneNumber = interactionSearchData?.phoneNumber || '';
            let record = !currentMatchedContact ? await matchContacts(phoneNumber) : currentMatchedContact;
            renderContactButton(record);
            let crmObjects = [getCrmObject(record)];
            return Promise.resolve({ crmObjects: crmObjects, screenPopObject: crmObjects[0] });
        },
        saveLog: function (params) {
            console.log({saveLog: params})
        },
        screenPop: function (params) {
            console.log({screenPop: params})
        },
        getTodayCallsCount: async function (params) {
            console.log({getTodayCallsCount: params})
            let count = await getCallsCount();
            return Promise.resolve(count);
        },
        getTodayChatsCount: function (params) {
            console.log({getTodayChatsCount: params})
            return Promise.resolve(0);
        },
        getTodayEmailsCount: function (params) {
            console.log({getTodayEmailsCount: params})
            return Promise.resolve(0);
        },
        openMyCallsToday: function (params) {
              console.log({openMyCallsToday: ALL_CALLS_VIEW})
              FAClient.navigateTo(ALL_CALLS_VIEW);
        },
        openMyChatsToday: function (params) {
            console.log({openMyChatsToday: params})
        },
        enableClickToDial: function (params) {
            console.log({enableClickToDial: params})
        },
        disableClickToDial: function (params) {
            console.log({disableClickToDial: params})
        }
    });

    interactionApi = window.Five9.CrmSdk.interactionApi();
    interactionApi.subscribe({
        callStarted: async function (params) {
            let inboundPhone = params?.callData?.ani;
            let callType = params?.callData?.callType;
            console.log({isOutbound, inboundPhone})
            if(!isOutbound && callType.includes('INBOUND')) {
                console.log({isOutbound, inboundPhone})
                let record = await matchContacts(inboundPhone);
                console.log({record})
                renderContactButton(record);
                FAClient.open();
                crmApi.objectVisited({crmObject: getCrmObject(record)});
            }
        },
        callFinished: function (params) {
            console.log({callFinished: params})
            FAClient.createEntity({
                entity: phoneAppletPrefixName,
                field_values: getCallLogValues(params)
            }, ({entity_value}) => {
                resetApplet();
                return FAClient.showModal('entityFormModal', {
                    entity: phoneAppletPrefixName,
                    entityLabel: 'Phone Call Outcome Log',
                    entityInstance: entity_value,
                    showButtons: false,
                });
            });
        },
        callAccepted: function (params) {
            ongoingCall = true;
            console.log({callAccepted: params})
        },
        callRejected: function (params) {
            console.log({callRejected: params})
        },
        callEnded: function (params) {
            console.log({callEnded: params})
        },
        emailOffered: function (params) {
        },
        emailAccepted: function (params) {
        },
        emailRejected: function (params) {
        },
        emailTransferred: function (params) {
        },
        emailFinished: function (params) {
        },
        chatOffered: function (params) {
        },
        chatAccepted: function (params) {
        },
        chatRejected: function (params) {
        },
        chatTransferred: function (params) {
        },
        chatEnded: function (params) {
        },
        chatFinished: function (params) {
        }
    });

    FAClient.on('onCall', ({record,phone}) => {
        isOutbound = true;
        setCurrentMatchedContact(record);
        const phoneNumber = phone;
        FAClient.open();
        renderContactButton(record);
        crmApi.click2dial({
            click2DialData: {
                clickToDialNumber: phoneNumber,
                //crmObject: getCrmObject(record),
                screenpopC2DSearch: true,
            },
        });
    });

    FAClient.on('phone_field_clicked', ({number, record, appName}) => {
        /*
        isOutbound = true;
        setCurrentMatchedContact(record);
        FAClient.open();
        renderContactButton(record);
        crmApi.click2dial({
            click2DialData: {
                clickToDialNumber: number,
                //crmObject: getCrmObject(record),
                screenpopC2DSearch: true,
            },
        });
         */
    });
}


function getCrmObject(record) {
    return {
        id: record?.seq_id,
        label: "Contact",
        name: record?.field_values?.full_name?.value || '',
        isWho: true,
        isWhat: false,
        fields:[
            { displayName: "Company", value: record?.field_values?.contact_field5?.display_value || '' }
        ]
    }
}

async function matchContacts(phoneNumber) {
    let variables = {
        entity: "contact",
        filters: [
            {
                field_name: 'work_phone',
                operator: 'contains',
                values: [phoneNumber],
            },
        ],
    };
    let contact = null;
    FAClient.listEntityValues(variables, (contacts) => {
            contact = contacts && contacts.length > 0 ? contacts[0] : null
            if(contact) {
                setCurrentMatchedContact(contact)
            }
        }
    );
    return new Promise(resolve => delay(1000).then(() => resolve(contact)))
}

async function getCallsCount() {
    let variables = {
        entity: "five9",
        filters: [{
            field_name: "created_at",
            operator: "period",
            values: ["Today"]
        }]
    };
    let calls = 0;
    FAClient.listEntityValues(variables, (callsRecords) => {
          calls = callsRecords && callsRecords.length > 0 ? callsRecords.length : null;
        }
    );
    return new Promise(resolve => delay(1000).then(() => resolve(calls)))
}

function getCallLogValues({ callData, callLogData, reason }) {
    return {
        [phoneAppletFields.fnId]: callData?.sessionId || '',
        [phoneAppletFields.contact]: currentMatchedContact?.id || '',
        [phoneAppletFields.from]: callData?.ani || '',
        [phoneAppletFields.to]: callData?.dnis || '',
        [phoneAppletFields.campaignName]: callData?.campaignName || '',
        [phoneAppletFields.direction]: isOutbound ? 'Outbound' : 'Inbound',
        [phoneAppletFields.note]: callLogData?.comment ? `<div>${callLogData?.comment}</div>` : '',
        [phoneAppletFields.disposition]: callLogData?.dispositionName || '',
        [phoneAppletFields.duration]: callLogData?.duration / 1000 || '',
        [phoneAppletFields.handleTime]: callLogData?.handleTime / 1000 || '',
        [phoneAppletFields.callEndReason]: reason || '',
    }
}

function renderContactButton(record=null) {
    record = record || currentMatchedContact;
    let url = "/entity/contact/view/all/create";
    let buttonValue = "Create New Contact";
    const footer = document.getElementById('footer');

    if (record){
        buttonValue = `Navigate to ${_.get(record,'field_values.full_name.value', '')}`;
        url = `/contact/view/${record.id}`
    }

    const footerButton = document.createElement('button');
    footerButton.innerText = buttonValue;

    footerButton.onclick = () => {
        FAClient.navigateTo(url);
    };

    footer.appendChild(footerButton);
}

function resetApplet() {
    const footer = document.getElementById('footer');
    footer.innerHTML = '';
    setCurrentMatchedContact(null);
    ongoingCall = false;
    isOutbound = false;
}