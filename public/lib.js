class FAAppletClient {
    constructor({ appletId } = {}) {
        this.registeredEvents = {};
        this.appletId = appletId;
        this.params = {};
        this.eventKey = `fa_applet[${appletId}]`;
        this.queryCallbacks = {};
        this.open = this.open;

        const urlParams = new URLSearchParams(window.location.search);
        for (const [key, value] of urlParams.entries()) {
            this.params[key] = value;
        }

        this._postMessageTop = (eventName, payload = {}, callback) => {
            const requestId = btoa(Date.now() * Math.random()).slice(9);
            if (typeof callback === 'function') {
                this.queryCallbacks[requestId] = callback;
            }
            const type = `${this.eventKey}.${eventName}`;
            window.top.postMessage(
                { type, payload, requestId },
                '*'
            );
        };

        function _eventHandlerMatches(eventName, eventType) {
            const splitType = eventType?.split('.')[1];
            return splitType === eventName;
        }

        function _parentListener(eventKey, event, queryCallbacks) {
            const { data } = event;
            if (!data) {
                return console.log('Wrong type of event sent');
            }
            switch (data.type) {
                case `${eventKey}.getTeamMembers_response`:
                    queryCallbacks[data.requestId](data.response);
                    break;
                case `${eventKey}.createEntity_response`:
                    queryCallbacks[data.requestId](data.response);
                    break;
                case `${eventKey}.updateEntity_response`:
                    queryCallbacks[data.requestId](data.response);
                    break;
                case `${eventKey}.upsertEntity_response`:
                    queryCallbacks[data.requestId](data.response);
                    break;
                case `${eventKey}.listEntityValues_response`:
                    queryCallbacks[data.requestId](data.response);
                    break;
                case `${eventKey}.globalSearch_response`:
                    queryCallbacks[data.requestId](data.response);
                default:
                    break;
            }
        }

        const _eventHandler = (event) => {
            const data = event.data;
            if (!data) {
                return console.error('Wrong type of event sent');
            }
            Object.keys(this.registeredEvents).map((eventName) => {
                const eventHandler = this.registeredEvents[eventName];
                if (_eventHandlerMatches(eventName, data.type)) {
                    return eventHandler(data.payload);
                }
            });
            _parentListener(`fa_applet[${appletId}]`, event, this.queryCallbacks);
        };

        window.addEventListener('message', _eventHandler);

        if (appletId) {
            this._postMessageTop('loaded', { appletId });
        }
    }

    getTeamMembers(payload, callback) {
        this._postMessageTop('getTeamMembers', payload, callback);
    }
    createEntity(payload, callback) {
        this._postMessageTop('createEntity', payload, callback);
    }
    updateEntity(payload, callback) {
        this._postMessageTop('updateEntity', payload, callback);
    }
    upsertEntity(payload, callback) {
        this._postMessageTop('upsertEntity', payload, callback);
    }
    listEntityValues(payload, callback) {
        this._postMessageTop('listEntityValues', payload, callback);
    }
    open() {
        this._postMessageTop('open');
    }
    globalSearch(search, callback) {
        this._postMessageTop('globalSearch', { search }, callback);
    }
    logActivity(payload, callback) {
        this._postMessageTop('logActivity', payload, callback);
    }
    navigateTo(url) {
        this._postMessageTop('navigateTo', { url });
    }
    on(eventName, callback) {
        this.registeredEvents[eventName] = callback;
    }
    showModal(modalName, modalProps) {
        this._postMessageTop('showModal', { modalName, modalProps });
    }
    hideModal(modalName) {
        this._postMessageTop('hideModal', { modalName });
    }
}
