// (function () {

const phoneAppletPrefixName = "five9";

const PHONE_FIELD_TYPES = [
    { field: 'work_phone', type: 'business' },
    { field: 'mobile_phone', type: 'mobile' },
    { field: 'home_phone', type: 'home' },
    { field: 'alternate_phone_1', type: 'other' },
];

const phoneAppletFields = {
    fnId: `${phoneAppletPrefixName}_field5`,
    contact: `${phoneAppletPrefixName}_field1`,
    from: `description`,
    to: `${phoneAppletPrefixName}_field0`,
    duration: `${phoneAppletPrefixName}_field10`,
    note: `${phoneAppletPrefixName}_field4`,
    direction: `${phoneAppletPrefixName}_field3`,
    disposition: `${phoneAppletPrefixName}_field7`,
    handleTime: `${phoneAppletPrefixName}_field6`,
    callEndReason: `${phoneAppletPrefixName}_field8`,
    campaignName : `${phoneAppletPrefixName}_field9`,
    owner: 'owner_id'
}
